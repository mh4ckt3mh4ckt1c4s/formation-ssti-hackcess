from jinja2 import Template

user = {
    "name": "mh4ck",
    "admin": True
}
hobbies = ["hacker", "Dormir", "Apprendre"]

template = ""
with open("exemple.jinja", "r") as f:
    template = f.read()
output = Template(template).render(user=user, hobbies=hobbies)
with open("exemple.html", "w") as f:
    f.write(output)