from urllib import request
from flask import Flask, request, render_template_string, make_response

app = Flask(__name__)

TOP_SECRET = "CTF{C3c1_3sT_T0p_s3cR3t}"
admin = False

template = """
<!DOCTYPE html>
<head>
    <meta encoding="utf-8">
</head>
<body>
    <h1>Bienvenue sur ce site web, {{ name }} !</h1>
    {% if admin %}
    <h2>Vous êtes admin. Voici votre secret ultra bien protégé : {{ secret }} </h2>
    {% endif %}
    <p>Les gens cools disent des choses sur notre site web. Voulez-vous dire des choses sur notre site web ? </p>
    <form method="POST">
    <input type="text" name="name" placeholder="Votre nom"><br>
    <textarea rows="5" cols="80" name="text"></textarea><br>
    <input type="submit" value="Afficher mon nom !">
    </form>
    {% if name != "étranger" %}
    <p>Vous êtes quelqu'un de cool, {{ name }} !</p>
    <p>Vous vouliez nous dire ceci :</p>
    REPLACEHERE
    {% endif %}
</body>
"""

@app.route('/')
def main():
    return """
    <a href='/level1'>Niveau 1</a>
    <a href='/level2'>Niveau 2</a>
    <a href='/level3'>Niveau 3 (bonus)</a>"""

@app.route('/level1', methods=['GET', 'POST'])
def level1():
    name = "étranger"
    templateOK = template
    if request.method == 'POST':
        name = request.form.get("name", "étranger")
        text = request.form.get("text", "Vous êtes pas bavard...")
        # On met directement le contenu dans le template pour assurer la gestion du HTML/CSS
        # Clairement il s'agit du moyen le plus sécurisé !
        templateOK = template.replace("REPLACEHERE", text)
        print("replace !")
    # rien ne peut arriver de grave, si ?
    return make_response(render_template_string(templateOK, name=name, secret=TOP_SECRET, admin=admin), 200)

@app.route('/level2', methods=['GET', 'POST'])
def level2():
    name = "étranger"
    templateOK = template
    if request.method == 'POST':
        name = request.form.get("name", "étranger")
        text = request.form.get("text", "Vous êtes pas bavard...")
        # sécurité maximale anti SSTI qui exécute du code
        text = text.replace("os", "")
        text = text.replace("system", "")
        text = text.replace("popen", "")
        text = text.replace("_", "")
        text = text.replace("[", "")
        text = text.replace("]", "")
        templateOK = template.replace("REPLACEHERE", text)
    return make_response(render_template_string(templateOK, name=name, secret=TOP_SECRET, admin=admin), 200)

@app.route('/level3', methods=['GET', 'POST'])
def level3():
    name = "étranger"
    templateOK = template
    if request.method == 'POST':
        name = request.form.get("name", "étranger")
        text = request.form.get("text", "Vous êtes pas bavard...")
        # Absolument impossible à bypass (ou pas ?)
        text = text.replace("{{", "")
        text = text.replace("}}", "")
        templateOK = template.replace("REPLACEHERE", text)
    return make_response(render_template_string(templateOK, name=name, secret=TOP_SECRET, admin=admin), 200)

if __name__ == "__main__":
    app.run(debug=True)
