# Formation SSTI pour [HACKCESS](https://hackcess.org) du 20 octobre 2022

Ce repo contient à la fois les slides, les exemples et le mini-TP que j'ai proposé pour une formation sur les Server Side Template Injection, pour une formation adressée au club de cybersécurité de l'IUT de Roanne HACKCESS.

Le détail de l'utilisation de ces ressources est ci-dessous. Seul Python (et Internet) est requis pour pouvoir expérimenter.

## Mise en place

Pour mettre en place votre environnement, téléchargez ce repo, soit sous forme zip, soit en le clonant via git :

```shell
git clone https://gitlab.com/mh4ckt3mh4ckt1c4s/formation-ssti-hackcess
cd formation-ssti-hackcess
```

Puis créez un virtualenv, activez-le et installez les ressources nécessaires :

```shell
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Exemple

Rendez-vous dans le dossier, et lancez simplement le fichier `exemple.py` :

```shell
cd exemple
python exemple.py
```

Puis ouvrez le fichier `exemple.html` nouvellement créé avec un navigateur et admirez le résultat.

Vous pouvez expérimenter en modifiant le template Jinja ou les variables Python, puis en relançant la commande.

## TP

Ce TP met en place un serveur Flask utilisant Jinja et étant vulnérable aux SSTI. **En aucun cas ce code ne doit être pris comme exemple pour quelque projet que ce soit, hors pure démonstration de cybersécurité**.

Pour lancer le serveur, rendez-vous dans le dossier puis lancez le fichier `app.py` :

```shell
cd TP
python app.py
```

Le serveur démarrera en local, en vous donnant une URL. Cliquez dessus pour ouvrir le site dans votre navigateur et expérimenter.

## Nettoyage

Une fois que vous avez fini d'expérimenter, commencer par tuer tout processus tournant encore (ctrl+C), puis sortez du virtualenv :

```shell
deactivate
```

Vous pouvez ensuite supprimer le repo de votre machine si vous le souhaitez.


# Références

- https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/Server%20Side%20Template%20Injection
- https://portswigger.net/research/server-side-template-injection
- https://book.hacktricks.xyz/pentesting-web/ssti-server-side-template-injection